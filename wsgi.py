from flask import Flask
application = Flask(__name__)

@application.route("/")
def hello():
    return "This is the beginning of the end"

if __name__ == "__main__":
    application.run()
